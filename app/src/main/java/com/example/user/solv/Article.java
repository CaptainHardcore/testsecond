package com.example.user.solv;

import android.graphics.Bitmap;

/**
 * Created by WHEREISMYBAS on 9/19/17.
 */

public class Article {
    String title;
    String text;
    Bitmap preview;

    public Article(String title, String text, Bitmap preview) {
        this.title = title;
        this.text = text;
        this.preview = preview;
    }
    public Article(){}

    public String getTitle() {
        return title;
    }

    public void setTitle(String title) {
        this.title = title;
    }

    public String getText() {
        return text;
    }

    public void setText(String text) {
        this.text = text;
    }

    public Bitmap getPreview() {
        return preview;
    }

    public void setPreview(Bitmap preview) {
        this.preview = preview;
    }
}
