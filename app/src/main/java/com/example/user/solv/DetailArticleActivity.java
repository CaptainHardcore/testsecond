package com.example.user.solv;

import android.content.Intent;
import android.graphics.BitmapFactory;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.design.widget.TabLayout;
import android.support.v4.app.Fragment;
import android.support.v7.app.AppCompatActivity;
import android.view.LayoutInflater;
import android.view.MenuItem;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import org.w3c.dom.Text;

/**
 * Created by WHEREISMYBAS on 9/19/17.
 */

public class DetailArticleActivity extends AppCompatActivity{
    TextView title;
    TextView text;
    ImageView mImageView;

    @Override
    protected void onCreate(@Nullable Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.detail_article);
        title = (TextView) findViewById(R.id.title_detail);
        text = (TextView) findViewById(R.id.text_detail);
        mImageView = (ImageView) findViewById(R.id.preview_detail);
        getSupportActionBar().setElevation(0);
        TabLayout tabLayout = (TabLayout)findViewById(R.id.tab_layout);
        tabLayout.setBackgroundColor(getResources().getColor(R.color.colorPrimary));
        getSupportActionBar().setDisplayHomeAsUpEnabled(true);

        tabLayout.addTab(tabLayout.newTab().setText("алкоголизм"));
        tabLayout.addTab(tabLayout.newTab().setText("наркомания"));
        tabLayout.addTab(tabLayout.newTab().setText("зависимость"));
        Intent intent = getIntent();
        int i = intent.getIntExtra("selected", 0);
        tabLayout.getTabAt(intent.getIntExtra("selected",0)).select();
        title.setText(intent.getStringExtra("title"));
        text.setText(intent.getStringExtra("text"));
        mImageView.setImageBitmap(BitmapFactory.decodeByteArray(intent.getByteArrayExtra("image"),0, intent.getByteArrayExtra("image").length));
    }





    @Override
    public boolean onOptionsItemSelected(MenuItem item) {
        switch (item.getItemId()) {
            // Respond to the action bar's Up/Home button
            case android.R.id.home:
              finish();
                return true;
        }
        return super.onOptionsItemSelected(item);
    }



}
