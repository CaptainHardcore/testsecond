package com.example.user.solv;

import android.content.Intent;
import android.graphics.Bitmap;
import android.os.Bundle;
import android.support.annotation.Nullable;
import android.support.v4.app.Fragment;
import android.support.v7.widget.LinearLayoutManager;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;

import java.io.ByteArrayOutputStream;

/**
 * Created by WHEREISMYBAS on 9/19/17.
 */

public class Drugs extends Fragment {
    RecyclerView mRecyclerView;
    @Nullable
    @Override
    public View onCreateView(LayoutInflater inflater, @Nullable ViewGroup container, @Nullable Bundle savedInstanceState) {
        View v = inflater.inflate(R.layout.fragment_drugs, container, false);
        mRecyclerView = v.findViewById(R.id.recycler_narco);
        LinearLayoutManager llm = new LinearLayoutManager(getContext());
        RecyclerAdapter adapter = new RecyclerAdapter(ArticlesGenerator.generateDrugArticles(getContext()), mOnClickListener);
        mRecyclerView.setLayoutManager(llm);
        mRecyclerView.setAdapter(adapter);
        return v;
    }
    View.OnClickListener mOnClickListener = new View.OnClickListener() {
        @Override
        public void onClick(View view) {
            Intent intent = new Intent(getContext(), DetailArticleActivity.class);
            Article article = (Article) view.getTag();
            intent.putExtra("title", article.getTitle());
            intent.putExtra("text", article.getText());
            ByteArrayOutputStream stream = new ByteArrayOutputStream();
            article.getPreview().compress(Bitmap.CompressFormat.PNG, 100, stream);
            byte[] byteArray = stream.toByteArray();
            intent.putExtra("image", byteArray);
            intent.putExtra("selected", 1);
            startActivity(intent);
        }
    };
}
