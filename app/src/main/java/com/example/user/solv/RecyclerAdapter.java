package com.example.user.solv;

import android.support.v7.widget.CardView;
import android.support.v7.widget.RecyclerView;
import android.view.LayoutInflater;
import android.view.View;
import android.view.ViewGroup;
import android.widget.ImageView;
import android.widget.TextView;

import java.util.ArrayList;
import java.util.zip.Inflater;

/**
 * Created by WHEREISMYBAS on 9/19/17.
 */

public class RecyclerAdapter extends RecyclerView.Adapter<RecyclerAdapter.ViewHolder> {

    ArrayList<Article> mArticles;
    View.OnClickListener mOnClickListener;

    public RecyclerAdapter(ArrayList<Article> articles, View.OnClickListener onClickListener) {
        mArticles = articles;
        mOnClickListener = onClickListener;
    }

    @Override
    public RecyclerAdapter.ViewHolder onCreateViewHolder(ViewGroup parent, int viewType) {
        View v = LayoutInflater.from(parent.getContext()).inflate(R.layout.item_article, parent, false);
        return new ViewHolder(v, mOnClickListener);
    }

    @Override
    public void onBindViewHolder(RecyclerAdapter.ViewHolder holder, int position) {
        holder.title.setText(mArticles.get(position).getTitle());
        holder.text.setText(mArticles.get(position).getText());
        holder.mImageView.setImageBitmap(mArticles.get(position).getPreview());
        holder.mCardView.setTag(mArticles.get(position));
    }

    @Override
    public int getItemCount() {
        return mArticles.size();
    }
    class ViewHolder  extends RecyclerView.ViewHolder{
        ImageView mImageView;
        TextView title;
        TextView text;
        CardView mCardView;
        public ViewHolder(View itemView, View.OnClickListener mOnclickLitener) {
            super(itemView);
            mImageView = itemView.findViewById(R.id.preview);
            title = itemView.findViewById(R.id.title_article);
            text = itemView.findViewById(R.id.text_article);
            mCardView = itemView.findViewById(R.id.card);
            mCardView.setOnClickListener(mOnclickLitener);

        }
    }
}
