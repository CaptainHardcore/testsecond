package com.example.user.solv;

import android.support.v4.app.Fragment;
import android.support.v4.app.FragmentManager;
import android.support.v4.app.FragmentPagerAdapter;

/**
 * Created by WHEREISMYBAS on 9/19/17.
 */

public class ViewPagerAdapter extends FragmentPagerAdapter {
    private String[] titles = new String[]{"алкоголизм", "наркомания", "зависимость"};


    public ViewPagerAdapter(FragmentManager fm) {
        super(fm);
    }

    @Override
    public Fragment getItem(int position) {

        switch (position){
            case 0:
                return new Alco();
            case 1:
                return new Drugs();
            case 2:
                return new Deps();
            default:
                return new Alco();
        }


    }


    @Override
    public int getCount() {
        return 3;
    }

    @Override
    public CharSequence getPageTitle(int position) {
        return titles[position];
    }
}